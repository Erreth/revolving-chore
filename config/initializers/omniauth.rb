OmniAuth.config.logger = Rails.logger
OmniAuth.config.full_host = "https://revolvingchore.com"

SERVICES = YAML.load(File.open("#{::Rails.root}/config/oauth.yml").read)

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :developer unless Rails.env.production?
  provider :facebook, SERVICES['facebook']['key'], SERVICES['facebook']['secret'], {:scope => "email,publish_stream"} # if SERVICES['github']
  provider :twitter, SERVICES['twitter']['key'], SERVICES['twitter']['secret'] # if SERVICES['twitter']
end