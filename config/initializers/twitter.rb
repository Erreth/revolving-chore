Twitter.configure do |config|
  config.consumer_key = SERVICES['twitter']['key']
  config.consumer_secret = SERVICES['twitter']['secret']
end