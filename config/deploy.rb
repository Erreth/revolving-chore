set :rvm_type, :system
# Choose a Ruby explicitly, or read from an environment variable.
set :rvm_ruby_string, 'ruby-2.0.0-p0@revolvingchore'
# set :rvm_ruby_string, ENV['GEM_HOME'].gsub(/.*\//,'') 
require "rvm/capistrano"
require 'bundler/capistrano'
set :whenever_command, "bundle exec whenever"
require "whenever/capistrano"

# Add RVM's lib directory to the load path.
$:.unshift(File.expand_path('./lib', ENV['rvm_path']))

set :application, "revolvingchore.com"
set :port, 8000
set :use_sudo, false
set :repository, "git@bitbucket.org:Erreth/revolving-chore.git"
set :scm, :git
set :user, "deploy"
set :ssh_options, { :forward_agent => true }
set :branch, "master"
set :deploy_via, :remote_cache
set :deploy_to, "/var/www/vhosts/#{application}"
set :rvm_type, :system

server application, :app, :web, :db, :primary => true

namespace :deploy do
  desc "Symlinks the database.yml"
  task :symlink_db do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end

  desc "Restart the Thin processes"
  task :restart do
    run <<-CMD
      cd /var/www/vhosts/revolvingchore.com/current; bundle exec thin restart -C /etc/thin/revolvingchore.yml
    CMD
  end
end

before 'deploy:assets:precompile', 'deploy:symlink_db'


# if you want to clean up old releases on each deploy uncomment this:
after "deploy:restart", "deploy:cleanup"
after "deploy", "deploy:migrate"
