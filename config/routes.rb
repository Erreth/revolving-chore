RevolvingChore::Application.routes.draw do
  match '/calendar(/:year(/:month))' => 'calendar#index', :as => :calendar, :constraints => {:year => /\d{4}/, :month => /\d{1,2}/}

  resources :groups do
    collection do
      post 'assigned_chores/add_more_assigned_chores'
    end 
    member do
      delete 'remove_user_from_group'
      get 'chore_history'
    end
    resources :assigned_chores do
      resources :ratings do
        collection do
          post 'upsert'
          post 'archive'
        end
      end
      resources :trades do
        member do
          post 'accept'
          post 'decline'
          post 'retract'
        end
      end
      collection do
        post 'create_initial_assigned_chores'
      end
      member do
        post 'complete'
        post 'uncomplete'
      end
    end
    resources :chores
    resources :invitations do
      member do
        get 'accept'
      end
    end
  end

  # resources :assigned_chores do
  #   collection do
  #     post 'add_more_assigned_chores'
  #   end
  # end

  resources :users do
    member do
      get 'chore_history'
    end
  end

  get "home/index"
  get "home/how_this_works"
  get "home/faq"
  get "home/login"
  
  root :to => "home#index"

  post '/oauth/request_token' => 'sessions#new'

  match '/auth/:provider/callback' => 'sessions#create',:as => 'login'
  match '/auth/failure' => 'sessions#failure'
  match '/auth/logout' => 'sessions#destroy',:as => 'logout'

  # Any other routes are handled here (as ActionDispatch prevents RoutingError from hitting ApplicationController::rescue_action).
  # match "*path", :to => "application#routing_error_path"
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
