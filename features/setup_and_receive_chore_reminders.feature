Feature: Setup and Receive Chore Reminders
  In order to alert myself about chore-related events
  As a user of RevolvingChore
  I want to be able to setup event alerts using email, SMS, and social media
  
Scenario: Setup Chore Reminders
  Given I am logged in to RevolvingChore
  And I am part of a group
  And I am on "my User Dashboard"
  When I click "User Preferences"
  And I select "Send me chore notifications"
  And I select ""
  
  