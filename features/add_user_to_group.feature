Feature: Add User To Group
	In order to add a user to a group
	As a user of RevolvingChore
	I want to be able to invite and add a user to a group

Background: I am registered, logged in, and part of a group
	Given I am logged in to RevolvingChore
	And I am part of "Lloyd House"
	And I am on "the Edit Group Page" for "Lloyd House"

Scenario: User sends an email invitation
	When I press "Add a New User"
	And I enter an email address on "the Edit Group Page"
	And I press "Add User"
	Given "Ben" is not a part of "Lloyd House"
	Then I should see "Invitation Sent" on "the Edit Group Page"

Scenario: User adds a new user to group
	Given I have invited "Ben"
	And "Ben" has not yet accepted the invitation
	When "Ben" accepts the invitation link, plus logs in to RevolvingChore
	Then "Ben" is added to "Lloyd House"