
require File.expand_path(File.join(File.dirname(__FILE__), "..", "support", "paths"))

Given /^I am logged in to RevolvingChore$/ do
  visit "/auth/developer"
  step 'I fill in "name" with "Fefe"'
  step 'I fill in "email" with "htouw@westmont.edu"'
  step 'I press "Sign In"'
end

When /^(?:|I )fill in "my (.*) credentials"$/ do |strategy|
  step 'I fill in "name" with "Fefe"'
  step 'I fill in "email" with "htouw@westmont.edu"'
end

Given /^I am on "([^"]*)" for "([^"]*)"$/ do |page, group|
  visit path_to page, :group_name => group
end

Given /^I am on "([^"]*)"$/ do |page|
  visit path_to page
end

When /^(?:|I )press "([^"]*)"$/ do |button|
  click_button(button)
end

When /^(?:|I )follow "([^"]*)"$/ do |link|
  click_link(link)
end

#Need to finish the step definition below

Then /^(?:|I )should see "([^"]*)"$/ do |text|
  if page.respond_to? :should
    page.should have_content(text)
  else
    assert page.has_content?(text)
  end
end

When /^I enter an email address on "([^"]*)"$/ do |page|
  #text field should allow email address to be entered
  #enter text field on the page
end

When /^"Ben" accepts the invitation link, plus logs in to RevolvingChore$/ do

  #check for cookie with invitation_id. Assert true if cookie is there

  visit '/login'
  fill_in 'username', :with => 'ben'
  fill_in 'password', :with => 'password'
  click_button 'Login'
  if page.respond_to? :should
    page.should have_content('Joined Group')
  else
    assert page.has_content?('Joined Group')
  end
end

When /^(?:|I )fill in the following:$/ do |fields|
  fields.rows_hash.each do |name, value|
    When %{I fill in "#{name}" with "#{value}"}
  end
end

Then /^I should be on "my User Dashboard"$/ do
  step 'I should see "Fefe\'s Dashboard"'
end

Then /^I should be on "([^"]*)" for "([^"]*)"$/ do |page, group|
  visit path_to page, :group_name => group
end

When /^(?:|I )fill in "([^"]*)" with "([^"]*)"$/ do |field, value|
  fill_in(field, :with => value)
end

Then /^I should be on "(.*)"$/ do
  visit path_to page
end


