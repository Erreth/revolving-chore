Given /^I am part of "Lloyd House"$/ do 
  
  unless @felix = User.find_by_username('Fefe')
    @felix = FactoryGirl.create(:user)
  end
  
  unless @felix.groups.where(:name => 'Lloyd House').name == 'Lloyd House'
    @lloyd = FactoryGirl.create(:group)
    @lloyd.users << @felix
  end
end

Given /^"Ben" is not a part of "Lloyd House"$/ do	
 	if ben = User.find_by_email('ben.vanderwall@gmail.com')
  	assert_nil ben.groups.where(:name => 'Lloyd House')
  else
  	true
  end
end

Given /^I have invited "Ben"$/ do
	assert Invitation.where(:group_name => 'Lloyd House', :email => 'ben.vanderwall@gmail.com')
end

Given /^"Ben" has not yet accepted the invitation$/ do
	if invitation = Invitation.where(:group_name => 'Lloyd House', :email => 'ben.vanderwall@gmail.com')
		assert invitation.accepted.invalid?
	else
		true
	end
end

Then /^"Ben" is added to "Lloyd House"$/ do
	ben_id = User.find_by_username('Ben Vanderwall').user_id
	lloyd_house_id = Inviataion.where(:group_name => 'Lloyd House', :email => 'ben.vanderall@gmail.com').group_id

	Group_Maps.new(:group_id => lloyd_house_id, :user_id => ben_id)

	assert Group.where(:group_id => lloyd_house_id).users(:user_id => ben_id)
end