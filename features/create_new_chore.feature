Feature: Create New Chore
  In order to add chores to the group
  As a user of RevolvingChore
  I want to be able to add a new chore
  
Background: I am registered, logged in, and part of a group
  Given I am logged in to RevolvingChore
  And I am part of "Lloyd House"
  And I am on "the Lloyd House Dashboard"
  
Scenario: User adds a chore to the group
  When I press "Add Chore"
  And I enter chore information
  And I press "Add Chore"
  Then I should see "Chore Created" on "the Group Dashboard"
  And I should see "Critique" on "the Group Dashboard"