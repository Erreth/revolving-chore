Feature: Create Group
  In order to create a new group
  As a user of RevolvingChore
  I want to be able to create a new group
  
Background: I am registered and logged in
  Given I am logged in to RevolvingChore
  
Scenario: Felix is redirected to Create Group Page
  Given I am on "Fefe's Dashboard"
  When I follow "Create a Group"
  Then I should be on "the Create Group Page"

Scenario: Felix creates a new group
  Given I am on "the Create Group Page"
  When I fill in the following:
  | Group Name            | Lloyd House |
  | Chore Period Length   | 7           |

  And I press "Create"
  Then I should be on "the Lloyd House Dashboard"
  And I should see "Group Created" on "the Group Dashboard"