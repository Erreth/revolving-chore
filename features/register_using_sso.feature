Feature: Register using 3rd Party Services
  In order to make sign-ups more efficient and integration easier
  As a user of RevolvingChore
  I want to be able to sign up using through my favorite social networking site

Scenario: Authenticate with Developer
  Given I am on "the Home Page"
  When I follow "Log In"
  And I fill in "my developer credentials"
  Then I should be on "my User Dashboard"
  And I should see "Welcome Felix"
    
Scenario: Authenticate with Facebook
  Given I am on "the Home Page"
  When I follow "Log In"
  And I fill in "my Facebook credentials"
  And I allow RevolvingChore requested privileges
  Then I should be on "my User Dashboard"
  And I should see "Welcome Felix"
  
Scenario: Authenticate with Twitter
  Given I am on "the Home Page"
  When I select "Sign Up through Twitter"
  And I fill in "my Twitter credentials"
  And I allow RevolvingChore requested privileges
  Then I should be on "my User Dashboard"
  And I should see "Welcome Felix"
  
  