def path_to(page_name, options = {})
  case page_name
    
  when /^the Lloyd House Dashboard$/
    group = Group.find_by_name('Lloyd House')
    "/groups/#{group.id}"
  when /^Fefe's Dashboard$/
    unless @user = User.find_by_username('Fefe')
      FactoryGirl.create(options[:user])
      @user = User.find_by_username('Fefe')
    end
    "/users/#{@user.id}"
  when /^the Home Page$/
    "/home/index"
  when /^the Edit Group Page$/
  	group = Group.find_by_name(options[:group_name])
  	"/groups/#{group.id}/edit"
  when /^the Create Group Page$/
    "/groups/new"
  end
end