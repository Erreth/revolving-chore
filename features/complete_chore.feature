Feature: Complete a Chore
  In order to track completed chores and hold users accountable
  As a user of RevolvingChore
  I want to be able to complete and verify a chore
  
Background: I am registered, logged in, and part of a group
  Given I am logged in to RevolvingChore
  And I am part of "Lloyd House"
  And I am on "the Group Dashboard" for "Lloyd House"
  
Scenario: user marks a chore as completed
  Given I have not completed my chore
  And the chore is not past due
  When I press "Complete Chore"
  Then I should see "Chore completed!" on "the Group Dashboard"
  
Scenario: user verifies a chore as completed
  Given I have setup chore verification
  And a chore has been completed but not verified
  And a chore is not past due
  When I press "Verify Chore"
  Then I should see "Chore verified!" on "the Group Dashboard"
  