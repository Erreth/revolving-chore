class ChoresController < ApplicationController
  def new
    @chore = Chore.new
    render(:partial => "form", :object => @chore) if request.xhr?
  end

  def index
    @chores = Chore.where(:group_id => @group.id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @chores }
    end
  end

  def show
    @chore = Chore.find_by_id_and_group_id(params[:id],@group.id)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @chore }
    end
  end 

  # GET /chores/1/edit
  def edit
    @chore = Chore.find_by_id_and_group_id(params[:id],@group.id)
    render(:partial => "form", :object => @chore) if request.xhr?
  end

  # POST /chores
  # POST /chores.json
  def create
    @chore = Chore.new(params[:chore])
    @group.chores << @chore

    respond_to do |format|
      if @chore.save
        AssignedChore.create_initial_assigned_chores(params[:group_id])
        flash[:notice] = "You have successfully created a chore"
        format.html { redirect_to group_path(params[:group_id]), notice: 'chore was successfully created.' }
        format.json { render json: @chore, status: :created }
      else
        flash[:error] = "Looks like something went wrong"
        format.html { render action: "new" }
        format.json { render json: @chore.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /chores/1
  # PUT /chores/1.json
  def update
    @chore = Chore.find(params[:id])

    respond_to do |format|
      if @chore.update_attributes(params[:chore])
        AssignedChore.create_initial_assigned_chores(params[:group_id])
        flash[:notice] = "You have successfully update your chore"
        format.html { redirect_to edit_group_path(params[:group_id]), notice: 'chore was successfully updated.' }
        format.json { head :no_content }
      else
        flash[:error] = "Looks like something went wrong"
        format.html { render action: "edit" }
        format.json { render json: @chore.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /chores/1
  # DELETE /chores/1.json
  def destroy
    @chore = Chore.find(params[:id])

    respond_to do |format|
      if @chore.destroy
        AssignedChore.delete_assigned_chores(params[:group_id])
        unless Group.find(params[:group_id]).chores.empty? 
          AssignedChore.create_initial_assigned_chores(params[:group_id])
        end
        flash[:notice] = "You have successfully deleted your chore"
        format.html { redirect_to edit_group_path(params[:group_id]) }
        format.json { head :no_content }
      else
        flash[:error] = "Looks like something went wrong"
      end
    end
  end
end


