class UsersController < ApplicationController
  skip_before_filter :validate_and_set_allowable_group
  
  def show
    if @current_user.assigned_chores.empty? != true
      @current_chores = @current_user.current_chores
    end

    @month = (params[:month] || (Time.zone || Time).now.month).to_i
    @year = (params[:year] || (Time.zone || Time).now.year).to_i

    @shown_month = Date.civil(@year, @month)

    @event_strips = @current_user.assigned_chores.event_strips_for_month(@shown_month)

    if @current_user.groups.empty? != true
      @event_strips = @current_user.set_calendar_colors(@event_strips)
    end
    
    @pending_trades = @current_user.pending_trades
    @user_chores_completed_by_week = @current_user.chores_completed_by_week
    @user_percentage_chores_completed_by_group = @current_user.percentage_chores_completed_by_group
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        flash[:notice] = "You have successfully updated your account!"
        format.html { redirect_to @user }
        format.json { head :no_content }
      else
        flash[:error] = "Looks like something went wrong."
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end
  
  def chore_history
    @chore_history = @current_user.chore_history
  end
end
