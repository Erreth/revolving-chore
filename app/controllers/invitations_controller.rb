class InvitationsController < ApplicationController
  skip_before_filter :validate_and_set_allowable_group, :only => :accept
  
  # GET /groups/new
  # GET /groups/new.json
  def new
    @invitation = Invitation.new
    render(:partial => "form", :object => @invitation) if request.xhr?
  end

  # POST /groups
  # POST /groups.json
  def create
    @invitation = Invitation.new(params[:invitation])
    @invitation.group = Group.find_by_id(params[:group_id])
    
    respond_to do |format|
      if @invitation.save
        # Send invitation email
        UserMailer.invite(@invitation).deliver
        flash[:notice] = "You have successfully sent an invite!"
        format.html { redirect_to group_path(params[:group_id]) }
        format.json { render json: @invitation, status: :created }
      else
        flash[:error] = "Looks like something went wrong"
        format.html { render action: "new" }
        format.json { render json: @invitation.errors, status: :unprocessable_entity }
      end
    end
  end

  def accept
    @invitation = Invitation.find_by_id(params[:id])
    @invitation.update_attribute(:accepted, true)
    @current_user.groups << @invitation.group
    AssignedChore.create_initial_assigned_chores(@invitation.group)
    redirect_to group_path(@invitation.group_id)
  end
end
