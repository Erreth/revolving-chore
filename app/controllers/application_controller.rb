class ApplicationController < ActionController::Base
  protect_from_forgery  
  before_filter :set_current_user, :except => :routing_error_path
  before_filter :set_current_env, :except => :routing_error_path
  before_filter :validate_and_set_allowable_group, :except => :routing_error_path

  layout :set_layout
  
  class AccessDenied < StandardError; end

  rescue_from AccessDenied, :with => :access_denied
  #rescue_from ActionController::RoutingError, :with => :routing_error
  
  # def routing_error_path
  #   raise ActionController::RoutingError.new(params[:path])
  # end
  
  protected
  def set_current_user
    @current_user ||= User.find_by_id(session[:user_id])
    unless @current_user
      cookies[:action] = request.fullpath
      redirect_to home_login_path and return
    end 
  end
  
  def set_current_env
    @current_env = Rails.env
  end
  
  def validate_and_set_allowable_group
    @group = @current_user.groups.find_by_id(params[:group_id] || params[:id])
    unless @group
      raise AccessDenied #redirect_to user_path(@current_user) and return
    end
  end
  
  private
  def access_denied
    render "errors/401", :status => 401
  end
  
  def routing_error
    render "errors/404", :status => 404
  end
  
  def set_layout
    if request.xhr?
      return false 
    else
      return "application"
    end
  end
end
