class RatingsController < ApplicationController
  def upsert
    rating = Rating.find_or_create_by_user_id_and_assigned_chore_id(@current_user.id, params[:assigned_chore_id])
    rating.update_attributes({:score => params[:rating][:score], :user_name => @current_user.username})
    
    respond_to do |format|
      format.html 
      format.json { render json: rating }
    end
  end
  
  def archive
    rating = Rating.find_or_create_by_user_id_and_assigned_chore_id(@current_user.id, params[:assigned_chore_id])
    rating.update_attributes(:archived => true)
    
    respond_to do |format|
      format.html 
      format.json { render json: rating }
    end
  end 
end