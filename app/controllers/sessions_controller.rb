class SessionsController < ApplicationController
  # user shouldn't have to be logged in before logging in
  skip_before_filter :set_current_user
  skip_before_filter :validate_and_set_allowable_group
  
  def create
    auth=request.env["omniauth.auth"]
    
    if @current_env == "development"
      auth[:uid] = auth[:info][:name]
      auth[:info][:email] = "cwatts@westmont.edu"
    end 
    
    user=User.find_and_update_with_omniauth(auth) ||
      User.create_with_omniauth(auth)
    
    session[:user_id] = user.id
    action = cookies[:action]
    
    unless action
      redirect_to user_path(user.id)
    else
      cookies.delete :action
      redirect_to action
    end
  end
  
  def destroy
    session.delete(:user_id)
    flash[:notice] = 'Logged out successfully.'
    redirect_to home_index_path
  end
end