class HomeController < ApplicationController
  # user shouldn't have to be logged in to visit the home page
  skip_before_filter :set_current_user
  skip_before_filter :validate_and_set_allowable_group
  
  def index
  end

  def how_this_works
  end

  def faq
  end

  def support
  end
  
  def login
  end
end
