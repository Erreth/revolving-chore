class TradesController < ApplicationController
  
  def new
    @trade = Trade.new
    @from_chore = AssignedChore.find_by_id(params[:assigned_chore_id])
    @possible_chore_trades = @group.possible_chore_trades(@current_user.id)
    render(:partial => "form", :object => @trade) if request.xhr?
  end
  
  def create
    @from_chore = AssignedChore.find_by_id(params[:assigned_chore_id])
    @to_chore = AssignedChore.find_by_id(params[:trade][:to_chore_id])
    
    @trade = Trade.new({
      :from_user_id => @from_chore.user_id,
      :from_user_name => @from_chore.username,
      :from_chore_id => @from_chore.id,
      :from_chore_name => @from_chore.name,
      :to_user_id => @to_chore.user_id,
      :to_user_name => @to_chore.username,
      :to_chore_id => @to_chore.id,
      :to_chore_name => @to_chore.name,
      :status => 'Pending',
      :group_id => @from_chore.group_id})
 
    respond_to do |format|
      if @trade.save
        # Send invitation email
        # UserMailer.invite(@invitation).deliver
        flash[:notice] = "You have successfully initiated a trade!"
        format.html { redirect_to group_path(params[:group_id]) }
        format.json { render json: @trade, status: :created }
      else
        flash[:error] = "Looks like something went wrong"
        format.html { render action: "new" }
        format.json { render json: @trade.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def accept
    @trade = Trade.find_by_id(params[:id])
    
    respond_to do |format|
      if @trade.accept
        flash[:notice] = "You have successfully accepted a trade!"
        format.html { redirect_to :back }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def decline
    @trade = Trade.find_by_id(params[:id])
    
    respond_to do |format|
      if @trade.decline
        flash[:notice] = "You have successfully declined a trade!"
        format.html { redirect_to :back }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def retract
    @trade = Trade.find_by_id(params[:id])
    
    respond_to do |format|
      if @trade.retract
        flash[:notice] = "You have successfully retracted a trade!"
        format.html { redirect_to :back }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end
end