class GroupsController < ApplicationController
  skip_before_filter :validate_and_set_allowable_group, :only => [:new, :create]
  # GET /groups/1
  # GET /groups/1.json
  def show

    unless @current_chores = @group.current_chores
      @users = @group.users
    end

    @month = (params[:month] || (Time.zone || Time).now.month).to_i
    @year = (params[:year] || (Time.zone || Time).now.year).to_i

    @shown_month = Date.civil(@year, @month)

    @event_strips = @group.assigned_chores.event_strips_for_month(@shown_month)
    @event_strips = @group.set_calendar_colors(@event_strips)
    
    @recently_completed_chores = @group.recently_completed_chores(@current_user.id)
    @pending_trades = @group.pending_trades
    @group_chores_completed_by_week = @group.chores_completed_by_week
    @group_percentage_chores_completed_by_user = @group.percentage_chores_completed_by_user
    @group_percentage_completed_by_chore = @group.percentage_completed_by_chore
  end 


  # GET /groups/new
  # GET /groups/new.json
  def new
    @group = Group.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @group }
    end
  end

  # GET /groups/1/edit
  def edit
  end

  # POST /groups
  # POST /groups.json
  def create
    params[:group][:chore_start_date] = Date.strptime(params[:group][:chore_start_date], "%m/%d/%Y") rescue nil
    @group = Group.new(params[:group])
    @group.users << User.find(@current_user.id)

    respond_to do |format|
      if @group.save
        flash[:notice] = "You have successfully created a group!"
        format.html { redirect_to @group }
        format.json { render json: @group, status: :created, location: @group }
      else
        flash[:error] = "Looks like something went wrong."
        format.html { render new_group_path }
        # format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /groups/1
  # PUT /groups/1.json
  def update
    params[:group][:chore_start_date] = Date.strptime(params[:group][:chore_start_date], "%m/%d/%Y") rescue nil
    respond_to do |format|
      if @group.update_attributes(params[:group])
        AssignedChore.create_initial_assigned_chores(params[:id])
        flash[:notice] = "You have successfully updated your group!"
        format.html { redirect_to @group}
        format.json { head :no_content }
      else
        flash[:error] = "Looks like something went wrong."
        format.html { render action: "edit" }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groups/1
  # DELETE /groups/1.json
  def destroy
    @group.destroy

    respond_to do |format|
      format.html { redirect_to groups_url }
      format.json { head :no_content }
    end
  end

  def remove_user_from_group
    @group.users.destroy(params[:user_id])

    if @group.users.empty?
      @group.assigned_chores.where('due_date >= ?', Date.today).destroy_all
      @group.destroy
      redirect_to @current_user
    else
      AssignedChore.create_initial_assigned_chores(params[:id])
      redirect_to edit_group_path(@group.id)
    end
  end
  
  def chore_history
    @chore_history = @group.chore_history
  end
end
