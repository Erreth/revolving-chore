class AssignedChoresController < ApplicationController

	def self.add_more_assigned_chores
		#adds more assigned chores to groups that have fewer than 8 more sets of assigned chores

		if groups = Group.where(:assigned_more_chores_date => Date.today)

			groups.each do |group|
				AssignedChore.add_more_chores(group.id)
			end
		end
	end


	def self.send_notification
		incompleted_chores = AssignedChore.where(:due_date => Date.today + 1, :status => 'Not Completed').includes(:user).where('users.email_notification' => true)
		
		incompleted_chores.each do |chore|
			chore.user.notify(chore)
		end
	end
	
	def complete
	  @assigned_chore = AssignedChore.find_by_id(params[:id])
	  respond_to do |format|
      if @assigned_chore.complete
        flash[:notice] = "You have successfully completed your chore!"
        format.html { redirect_to :back }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
	end
	
	def uncomplete
	  @assigned_chore = AssignedChore.find_by_id(params[:id])
    respond_to do |format|
      if @assigned_chore.uncomplete
        flash[:notice] = "You have successfully reverted your chore!"
        format.html { redirect_to :back }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
	end

	def self.validate_chore_completion
		# finds users who 1. did not do their chore 2. and the chore is past due
    if incompleted_chores = AssignedChore.where(:status => 'Not Completed', :due_date => Date.today-1)
			incompleted_chores.each do |chore|
			  
			  # Expire Trades for this chore
			  chore.from_trades.each do |trade|
			    trade.expire
			  end
			  chore.to_trades.each do |trade|
			    trade.expire
			  end
			  
			  # Attempt to punish user for this chore
			  chore.group.punish(chore)
			end
		end	
	end
end