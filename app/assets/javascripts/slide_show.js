jQuery(document).ready(function($) {
  var currentPane = 0;
  var timer = 0;
  var numberOfPanes = 0;
  var start = true;


  function countNumberOfPanes() {
    return $('.home_page_slide_show_panes').children().length;
  };

  function displayPane(nextPaneIndex) {
    // fade in and out the pane and pane indicator
    $('.selected_pane_indicator').removeClass('selected_pane_indicator');
    $('#indicator' + nextPaneIndex.toString()).addClass('selected_pane_indicator');
    $('.selected_pane').fadeOut(200, function() {
      $('.selected_pane').removeClass('selected_pane');
      $('#pane' + nextPaneIndex.toString()).fadeIn(600);
      $('#pane' + nextPaneIndex.toString()).addClass('selected_pane');
    });

    // reset the timer
    clearInterval(timer);
    startTimer();

    currentPane = nextPaneIndex;
  };

  function displayNextPane() {
    nextPane = (currentPane + 1) % numberOfPanes;
    displayPane(nextPane);
  };

  function startTimer() {
    timer = setInterval(displayNextPane, 5000);
  };

  $('.pane_indicator').click(function() {
    nextPaneIndex = $(this).index();
    displayPane(nextPaneIndex);
  });


  if(start == true) {
    startTimer();
    numberOfPanes = countNumberOfPanes();
    start = false;
  };

});


// var FeaturedSlideshow = {
//   currentIndex: 0,
//   autoscrollTimer: null,
//   currentAnimation: null,
//   numPanes: 0,
  
//   initialize: function(numPanes) {
//     var self = this;
    
//     this.numPanes = numPanes;
    
//     // Autoscroll
//     this.autoscrollTimer = setInterval(function() {self.displayNextPage();}, 7000);

//     // Setup paging indicators
//     var indicators = $('.widget-featuredslideshow-pageindicators');
//     var indicatorsWidth = numPanes * 12 + (numPanes - 1) * 8;
//     var indicatorLeft = ($('.widget-featuredslideshow').width() - indicatorsWidth) / 2;
//     for(var i=0; i < numPanes; i++) {
//       var indicator = $('<span></span>');
//       indicator.addClass('widget-featuredslideshow-pageindicator');
//       if(i==0) {
//         indicator.addClass('widget-featuredslideshow-pageindicator-selected');
//       }
//       indicator.css({left: indicatorLeft + 'px'});
//       indicator.attr('index',i);
//       indicator.bind('click', function(event) {
//         if(self.autoscrollTimer) {
//           clearInterval(self.autoscrollTimer);
//           self.autoscrollTimer = null;
//         }
//         var elem = $(this);
//         if(elem.attr('index') != self.currentIndex) {
//           self.displayPageNumber(elem.attr('index'));
//         }
//       });
//       indicator.bind('mouseover', function(event) {
//         $(event.target).addClass('widget-featuredslideshow-pageindicator-selected');
//       });
//       indicator.bind('mouseout', function(event) {
//         var elem = $(event.target);
//         if(elem.attr('index') != self.currentIndex) {
//           elem.removeClass('widget-featuredslideshow-pageindicator-selected');
//         }
//       });
//       indicators.append(indicator);
//       indicatorLeft += 20;
//     }
    
//     // Setup prev/next arrows
//     $('#widget_featuredslideshow .widget-featuredslideshow-leftarrow').bind('click', function() {
//       if(self.autoscrollTimer) {
//         clearInterval(self.autoscrollTimer);
//         self.autoscrollTimer = null;
//       }
//       self.displayPreviousPage();
//     })
//     $('#widget_featuredslideshow .widget-featuredslideshow-rightarrow').bind('click', function() {
//       if(self.autoscrollTimer) {
//         clearInterval(self.autoscrollTimer);
//         self.autoscrollTimer = null;
//       }
//       self.displayNextPage();
//     })
    
//   },

//   displayNextPage: function() {
//     var nextIndex = (parseInt(this.currentIndex) + 1) % this.numPanes;
//     this.displayPageNumber(nextIndex);
//   },
  
  
//   displayPreviousPage: function() {
//     var prevIndex = (parseInt(this.currentIndex) + parseInt(this.numPanes) - 1) % this.numPanes;
//     this.displayPageNumber(prevIndex);
//   },
  
  
//   displayPageNumber: function(index) {
//     // Set page indicators
//     var indicators = $('#widget_featuredslideshow .widget-featuredslideshow-pageindicator');
//     for(var i=0; i < indicators.length; i++) {
//       var indicator = $(indicators[i]);
//       if(indicator.attr('index') == index) {
//         indicator.addClass('widget-featuredslideshow-pageindicator-selected');
//       } else {
//         indicator.removeClass('widget-featuredslideshow-pageindicator-selected');
//       }
//     }
    
//     // Scroll container panel
//     var pane = $($('.widget-featuredslideshow-pane')[index]);
//     var leftMargin = ($('#widget_featuredslideshow').width() - pane.width()) / 2;
//     var panels = $('.widget-featuredslideshow-panes');
//     var self = this;
//     panels.animate({"left":leftMargin - pane.width() * index + 'px'},
//                    300,
//                   function() {
//                     // Hide all desc panels
//                     $('.widget-featuredslideshow-descbox').hide();
                    
//                     // Fade in description panel and preload next pane.
//                     $(pane).find('.widget-featuredslideshow-descbox').fadeIn(300);
//                     self.currentAnimation = null;
//                   });

//     this.currentIndex = index;
//   }

// }
