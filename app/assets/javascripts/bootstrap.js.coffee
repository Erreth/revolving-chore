jQuery -> 
  Page.setup()

Modal =
  setup: ->
    $(".modalCandidate").click Modal.create
    $("<div aria-hidden='true' aria-labelledby='myModal' class='modal hide fade' id='myModal' role='dialog' tabindex='-1'><div class='modal-header'><button aria-hidden='true' class='close' data-dismiss='modal' type='button'>×</button><h1 id='myModal'></h1></div><div class='modal-body'>").appendTo $("body")  if $(".modal").length is 0 
      
  create: ->
    $("h1#myModal").text($(this).text())
    $.ajax
      type: "GET"
      url: $(this).attr("href")
      timeout: 5000
      success: Modal.populate
      error: ->
        alert "Edit Page Error"

    $("#myModal").modal "show"
    false

  populate: (data) ->
    $(".modal-body").html data
    $(".modal-body form").submit ->
      $("#myModal").modal "hide"
      valuesToSubmit = $(this).serialize()
      $.ajax
        type: "POST"
        url: $(this).attr("action")
        data: valuesToSubmit
        dataType: "JSON"
        success: Page.refresh
        error: ->
          alert "Posting Error"
      false
      
Button = 
  setup: ->
    $(".ajaxCandidate").click Button.submit
    
  submit: ->
    valuesToSubmit = $(this).serialize()
    if $(this).attr("data-confirm") is undefined or confirm("Are you sure?")
      $.ajax
        type: $(this).attr("data-method")
        url: $(this).attr("href")
        data: valuesToSubmit
        dataType: "JSON"
        success: Page.refresh
        error: ->
          alert "Posting Error"
    false
    
Rating = 
  setup: ->
    $('.star').raty
      scoreName: "rating[score]"
      score: ->
        $(this).attr "data-score"
      click: (score, evt) ->
        form = $(this).parents("form:first")
        valuesToSubmit = form.serialize()
        $.ajax
          type: "POST"
          url: form.attr("action")
          data: valuesToSubmit
          dataType: "JSON"
          success: Page.refresh
          error: ->
            alert "Posting Error"
  
Page =  
  refresh: ->
    $.ajax
      type: "GET"
      url: window.location.pathname
      timeout: 5000
      success: Page.populate
      error: ->
        alert "Refresh Error"
        
  populate: (data) ->
    index = $(".remember-tab > .active").index()
    $("#main").html data
    $(".remember-tab li:eq("+index+") a").tab("show")
    Page.setup()
  
  setup: ->
    document.cookie = 'time_zone='+jstz.determine().name()+';';
    $("a[rel=popover]").popover()
    $(".tooltip").tooltip()
    $("a[rel=tooltip]").tooltip()
    $('.datepicker').datepicker()
    $(".parent").change ->
      $(this).parent().nextAll(".child").first().toggle()
    $ Rating.setup
    $ Modal.setup
    $ Button.setup
    
  message: ->
   

  
