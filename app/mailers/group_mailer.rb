class GroupMailer < ActionMailer::Base
  
  layout 'email'
  default :from => "punishments@revolvingchore.com"
  
  def email_penalty(chore)
    @chore = chore
    @message = chore.group.email_penalty_msg
    mail(:to => chore.group.email_penalty_address, :subject => "Punishment for #{chore.username} on RevolvingChore")
  end
end
