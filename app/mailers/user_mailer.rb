class UserMailer < ActionMailer::Base
  layout 'email'
  default :from => "notifications@revolvingchore.com"
  
  def invite(invitation)
    @invitation = invitation
    mail(:to => invitation.email, :subject => "Join #{@invitation.group.name} on RevolvingChore")
  end
  
  def notify(upcoming_assigned_chore)
    @upcoming_assigned_chore = upcoming_assigned_chore
    mail(:to => @upcoming_assigned_chore.user.email, :subject => "Make sure to do your chore")
  end
end
