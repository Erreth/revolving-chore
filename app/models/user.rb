class User < ActiveRecord::Base
  has_many :group_maps
  has_many :groups, :through => :group_maps
  has_many :assigned_chores
  has_many :from_trades, :class_name => "Trade", :foreign_key => "from_user_id"
  has_many :to_trades, :class_name => "Trade", :foreign_key => "to_user_id"
  extend TwilioHelper
  
  attr_accessible :id, :username, :email, :display_name, :monetary_amount_due, 
    :auth_token, :auth_secret, :auth_expiration_date,
    :email_notification, :sms_notification, :phone_number
  
  def self.create_with_omniauth(auth)
    new_user = User.new()
    new_user.provider = auth["provider"]
    new_user.uid = auth["uid"]
    new_user.auth_token = auth["credentials"]["token"]
    new_user.auth_secret = auth["credentials"]["secret"]
    new_user.auth_expiration_date = Time.at(auth["credentials"]["expires_at"]) if auth["credentials"]["expires_at"]
    new_user.username = auth["info"]["name"]
    new_user.email = auth["info"]["email"].presence || auth["extra"]["raw_info"]["email"].presence
    new_user.save
    return new_user
  end
  
  def self.find_and_update_with_omniauth(auth)
    user = find_by_uid_and_provider(auth["uid"],auth["provider"])
    user.update_attributes(:auth_token => auth["credentials"]["token"], 
      :auth_secret => auth["credentials"]["secret"], 
      :auth_expiration_date => (Time.at(auth["credentials"]["expires_at"]) if auth["credentials"]["expires_at"]), 
      :username => auth["info"]["name"].presence || user.username, 
      :email => auth["info"]["email"].presence || auth["extra"]["raw_info"]["email"].presence || user.email) if user
    user
  end
  
  def post(message)
    if self.provider == "facebook"
      user = Koala::Facebook::API.new(self.auth_token)
      user.put_wall_post(message)
    elsif self.provider == "twitter"
      user = Twitter::Client.new(:oauth_token => self.auth_token, :oauth_token_secret => self.auth_secret)
      user.update(message) # Docs use Thread.new(expression)
    end
  end
  
  def notify(upcoming_assigned_chore)
    if self.email_notification && self.email
      UserMailer.notify(upcoming_assigned_chore).deliver
    end
    if self.sms_notification && self.phone_number
      # Send Text
      User.text(self.phone_number,"#{upcoming_assigned_chore.name} is due tomorrow")
    end
  end
  
  
  def current_chores
    assigned_chores = self.assigned_chores
    unless assigned_chores.empty?
      current_chores = assigned_chores.joins(:group).where("start_date <= ? and due_date >= ?", Date.today, Date.today).order('due_date ASC, name ASC')
    else
      return nil
    end
  end
  
  def pending_trades
    pending_trades = self.from_trades.where(:status => 'Pending') + self.to_trades.where(:status => 'Pending')
    unless pending_trades.empty?
      pending_trades
    else
      nil
    end
  end
  
  def chore_history
    self.assigned_chores.where('due_date < ?', Date.today).where("due_date > ?", Date.today - 3.month).order('due_date ASC, name ASC')
  end
  
  def chores_completed_by_week
    self.assigned_chores.select("date_trunc('week',due_date) as date,COUNT(completion_date) as count").where("due_date < ?", Time.now).where("completion_date is not null").group("date_trunc('week',due_date)").order("date_trunc('week',due_date) ASC")
  end
  
  def percentage_chores_completed_by_group
    self.assigned_chores.select("group_name, ROUND(COUNT(completion_date)/COUNT(id)::decimal * 100,2) as percent").where("due_date < ?", Time.now).group("group_name")    
  end
  
  def set_calendar_colors(event_strips)
    #returns hash of groups assigned to colors. Key => Value. groupname => color

    groups = self.groups
    colors = ["#E23939", "#55C6EB", "#3DB638", "#F09C37", "#E4E43D", "#4E4EF7", "#985AF5", "#74DFB0"]
    counter = 0
    group_colors = Hash.new

    groups.each do |group|
      group_colors.merge!(group.name => colors[counter % colors.length])
      counter += 1
    end

    event_strips.collect! do |event_strip|
      event_strip.collect! do |event|
        if event != nil && group_colors.has_key?(event.group_name)
          event.color = group_colors[event.group_name]
          event
        end
      end
    end
    event_strips
  end
end