class Trade < ActiveRecord::Base
  belongs_to :group
  belongs_to :from_user, :class_name => "User", :foreign_key => "from_user_id"
  belongs_to :to_user, :class_name => "User", :foreign_key => "to_user_id"
  belongs_to :from_chore, :class_name => "AssignedChore", :foreign_key => "from_chore_id"
  belongs_to :to_chore, :class_name => "AssignedChore", :foreign_key => "to_chore_id"
  
  attr_accessible :accept_date, :expiration_date, :from_chore_id, :from_chore_name, :from_user_id, :from_user_name, :status, :to_chore_id, :to_chore_name, :to_user_id, :to_user_name, :group_id
  
  def accept
    self.update_attributes(:status => "Accepted", 
        :accept_date => DateTime.now)
    self.from_chore.switch_user(self.to_user_id,self.to_user_name)
    self.to_chore.switch_user(self.from_user_id,self.from_user_name)
    self.update_related_trades
  end
  
  def decline
    self.update_attributes(:status => "Declined")
  end
  
  def retract
    self.update_attributes(:status => "Retracted")
  end
  
  def update_related_trades
    # Expire all other trades where no longer owner
    Trade.where("from_chore_id = ? AND status = ?", self.to_chore_id, "Pending").update_all(status: 'Expired')
    Trade.where("from_chore_id = ? AND status = ?", self.from_chore_id, "Pending").update_all(status: 'Expired')
    # Update all other trades where new recipient
    Trade.where("to_chore_id = ? AND status = ?", self.to_chore_id, "Pending").update_all(to_user_id: self.from_user_id, to_user_name: self.from_user_name)
    Trade.where("to_chore_id = ? AND status = ?", self.from_chore_id, "Pending").update_all(to_user_id: self.to_user_id, to_user_name: self.to_user_name)
  end
  
  def expire
    self.update_attributes(:status => "Expired")
  end
end
