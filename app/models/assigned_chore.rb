class AssignedChore < ActiveRecord::Base
  belongs_to :user
  belongs_to :group
  belongs_to :chore
  has_many :from_trades, :class_name => "Trade", :foreign_key => "from_chore_id"
  has_many :to_trades, :class_name => "Trade", :foreign_key => "to_chore_id"
  has_many :ratings
  attr_accessible :description, :due_date, :monetary_penalty_value, :name, :posted_on_facebook, :start_date, 
  								:status, :chore_period_length_days, :next_rotation_date, :chore_sort_type, :verified_by,
                  :group_id, :user_id, :username, :start_at, :end_at, :color, :completion_date
  
  has_event_calendar :start_at_field  => 'start_date', :end_at_field => 'due_date', :color_field => 'color'
  
  def complete
    self.update_attributes(:status => "Completed", 
        :completion_date => DateTime.now)
    self.from_trades.update_all(status: 'Chore Completed')
  end
  
  def uncomplete
    self.update_attributes(:status => "Not Completed",
      :completion_date => nil)
  end
  
  def rating(user_id)
    self.ratings.where("user_id = ?", user_id)[0].try(:score)
  end
  
  # def avg_rating
    # self.ratings.
  # end
  
  def self.find_insert_date(insert_type, group_id)
  	#looks for the initial date to start creating assigned_chore records
    group = Group.find(group_id)
    case insert_type

  	when 'create_initial_chores'
      if group.chore_start_date >= Date.today
        insert_date = group.chore_start_date
      elsif most_recent_assigned_chore = AssignedChore.where(:group_id => group.id).order("start_date desc").where(["start_date <= ?", Date.today]).first
				#Checks for if there is a current assigned chore. If there is, it sets the insert_date to the start date of the current assigned chore.
        insert_date = most_recent_assigned_chore.start_date
      else
        insert_date = Date.today
			end
		when "add_more_chores"
			last_assigned_chore = AssignedChore.where(:group_id => group.id).order("due_date desc").first
			last_assigned_chore.due_date + 1
    end
  end

  def self.delete_assigned_chores(group_id)
  	#delete current and future assigned chores, but not past
    group = Group.find(group_id)
    unless group.assigned_chores.empty?
      group.assigned_chores.where("due_date >= ?", Date.today).destroy_all
    end
  end


  def self.create_chore_order(group_id)
    # Returns an array of the chore order.
    # Note that the number of elements in the array will match number of users in the group. This creates a chore order.

		group = Group.find(group_id)

    chore_count = group.chores.length
    user_count = group.users.length
    chore_ids = Array.new
    chore_names = Hash.new
    list_of_chores = Array.new

    group.chores.each do |chore|
      list_of_chores << chore.id
      chore_names.merge!(chore.id => chore.name)
    end

    if chore_count == user_count
      list_of_chores.each do |chore_id|
        chore_ids << chore_id
      end
      chore_ids
      # Example: chore_ids == [34, 35, 65, 23], which represents chore ids for [mop, sweep, kitchen, dust]
    elsif chore_count > user_count
      while list_of_chores[0] != nil
        until chore_ids.length >= user_count
          chore_ids << list_of_chores.pop
        end
        chore_ids.each do |chore|
          index = chore_ids.index(chore)
          if chore_ids[index].class != Array && list_of_chores[0] != nil
            chore_ids[index] = [chore_ids[index],list_of_chores.pop]
          elsif list_of_chores[0] != nil
            chore_ids[index] = chore_ids[index] << list_of_chores.pop
          end      
        end
        chore_ids
      end
      # Example: chore_ids == [[34, 37], [35, 46], 49, 50 ], which represent chore ids for [[mop, sweep], [trash, dishes], dust, vacuum] 
      # case when chore_count > user_count. This gives some users more than 1 chore
    elsif chore_count < user_count
      chore_names.merge!(0 => 'No Chore')
      while chore_count < user_count
        list_of_chores << 0
        chore_count += 1
      end
      list_of_chores.each do |chore_id|
        chore_ids << chore_id
      end
      chore_ids
      # Example: chore_ids == [34, 35, 65, 23], which represents chore ids for [mop, sweep, no chore, no chore] 
      # case when chore_count < user_count. This assigns some users with 'No Chore'
    end
    chore_order = [chore_ids, chore_names]
    #returns an array of chore_id's, and a Hash of chore_names{id => name}
  end



  def self.assign_chores(group_id, chore_order, insert_date)
  	#creates one full set of assigned chores starting from the insert date 
  	group = Group.find(group_id)
  	users = Array.new
    usernames = Array.new
    chore_ids = chore_order[0]
    chore_names = chore_order[1]
    group_name = group.name
    group.users.each do |user|
    	users << user.id
      usernames << user.username
    end

    monetary_penalty_value = group.monetary_penalty_value
    chore_period_length = group.chore_period_length_days
    one_cycle_length = chore_period_length - 1
    cycles = users.length

    sql_array = Array.new

		users.each do |user|
			user_index = users.index(user)
			temp_date = insert_date
			chore_ids.rotate(user_index).each do |chore|
				if chore.class == Fixnum #only 1 chore is assigned to the user
	  			sql_array << create_assigned_chore_sql_string(group_id, user, usernames[user_index], chore, temp_date, one_cycle_length, monetary_penalty_value, chore_names[chore], group_name)
		  		temp_date += chore_period_length
	  		else #chore is an array. This assigns multiple chores to 1 user
	  			chore.each do |x|
            index = chore.index(x)
            sql_array << create_assigned_chore_sql_string(group_id, user, usernames[user_index], x, temp_date, one_cycle_length, monetary_penalty_value, chore_names[x], group_name)
		  		end
          temp_date += chore_period_length
	  		end
			end
		end

    sql_string = "INSERT INTO assigned_chores (user_id, group_id, chore_id, name, monetary_penalty_value, \
      status, posted_on_facebook, start_date, due_date, created_at, updated_at, username, group_name) VALUES #{sql_array.join(", ")}"
    ActiveRecord::Base.connection.execute(sql_string)
	end

  def self.create_assigned_chore_sql_string(group_id, user_id, username, chore_id, start_date, one_cycle_length, monetary_penalty_value, chore_name, group_name)
    unless penalty_value = monetary_penalty_value
      penalty_value = 0
    end
    "(#{user_id}, #{group_id}, #{chore_id}, #{ActiveRecord::Base.connection.quote(chore_name)}, #{penalty_value}, 'Not Completed', \
      false,'#{start_date}', '#{start_date + one_cycle_length}', '#{Time.now}', '#{Time.now}', #{ActiveRecord::Base.connection.quote(username)}, #{ActiveRecord::Base.connection.quote(group_name)})"
  end

  def self.create_initial_assigned_chores(group_id)
    
    #Make sure to only call this if the group has at least 1 user and 1 chore, else redirect to groups dashboard saying to add user or chore
    group = Group.find(group_id)
    insert_date = find_insert_date('create_initial_chores', group_id)
    chore_order = create_chore_order(group.id)
    cycles = chore_order[0].length #number of cycles in one chore period. ex. 7 users, 7 chores = 7 cycles
    chore_period_length = group.chore_period_length_days

    delete_assigned_chores(group.id)

    if cycles <= 7     
      group.update_attribute(:assign_more_chores_date, (insert_date + (2 * chore_period_length)))
      multiplier = (10 / cycles).ceil
      multiplier.times do 
        assign_chores(group.id, chore_order, insert_date)
        insert_date += (cycles * chore_period_length)
      end
    else #8 or more chores
      assign_chores(group.id, chore_order, insert_date)
      group.update_attribute(:assign_more_chores_date, (insert_date + ((cycles - 7) * chore_period_length)))
    end
  end

  def self.add_more_chores(group_id)
    #inser_date = find the last due date and add 1 day
    group = Group.find(group_id)
    insert_date = find_insert_date('add_more_chores', group_id)
    chore_order = create_chore_order(group_id)
    cycles = chore_order[0].length #number of cycles in one chore period. ex. 7 users, 7 chores = 7 cycles
    chore_period_length = group.chore_period_length_days

    assign_chores(group_id, chore_order, insert_date)

    group.update_attribute(:assign_more_chores_date, Date.today + (chore_period_length * cycles))
  end

  def clip_range(start_d, end_d)
    # make sure we are comparing date objects to date objects,
    # otherwise timezones can cause problems
    start_at_d = start_at.to_date
    end_at_d = end_at.to_date
    # Clip start date, make sure it also ends on or after the start range
    if (start_at_d < start_d and end_at_d >= start_d)
      clipped_start = start_d
    else
      clipped_start = start_at_d
    end

    # Clip end date
    if (end_at_d > end_d)
      clipped_end = end_d
    else
      clipped_end = end_at_d
    end
  
  [clipped_start, clipped_end]
  end
  
  def switch_user(user_id,user_name)
    self.update_attributes(:user_id => user_id,
      :username => user_name)
  end

end












