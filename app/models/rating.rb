class Rating < ActiveRecord::Base
  belongs_to :user
  belongs_to :assigned_chore
  
  attr_accessible :score, :user_name, :archived
end