class Chore < ActiveRecord::Base
  belongs_to :group
  has_many :assigned_chores
  attr_accessible :description, :name, :group_id, :color

end