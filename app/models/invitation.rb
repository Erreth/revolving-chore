class Invitation < ActiveRecord::Base
  belongs_to :group
  attr_accessible :email, :accepted, :invitee_name
end
