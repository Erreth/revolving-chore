class GroupMap < ActiveRecord::Base
  belongs_to :user
  belongs_to :group
  attr_accessible :title, :body, :user_id, :group_id
end
