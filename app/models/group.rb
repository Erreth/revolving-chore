class Group < ActiveRecord::Base
  has_many :chores
  has_many :group_maps
  has_many :users, :through => :group_maps
  has_many :assigned_chores
  has_many :invitations
  has_many :trades
  attr_accessible :name, :chore_start_date, :time_zone, :chore_period_length_days, 
    :facebook_penalty, :facebook_penalty_msg, 
    :monetary_penalty, :monetary_penalty_value,
    :email_penalty, :email_penalty_address, :email_penalty_msg 

  validates :name, :presence => true
  validates :chore_period_length_days, :presence => true
  validates :chore_period_length_days, :numericality => {:greater_than_or_equal_to => 0}
  validates_numericality_of :chore_period_length_days, :only_integer => true

  validates :chore_start_date, :presence => true

  def set_calendar_colors(event_strips)
    #returns hash of colors assigned to user. Key => Value. username => color

    users = self.users
    colors = ["#E23939", "#55C6EB", "#3DB638", "#F09C37", "#E4E43D", "#4E4EF7", "#985AF5", "#74DFB0"]
    counter = 0
    user_colors = Hash.new

    users.each do |user|
      user_colors.merge!(user.username => colors[counter % colors.length])
      counter += 1
    end

    event_strips.collect! do |event_strip|
      event_strip.collect! do |event|
        if event != nil && user_colors.has_key?(event.username)
          event.color = user_colors[event.username]
          event
        end
      end
    end

    event_strips

  end

  def current_chores
    assigned_chores = self.assigned_chores
    unless assigned_chores.empty?
      current_chores = assigned_chores.where("start_date <= ? and due_date >= ?", Date.today, Date.today).order('username ASC, name ASC')
    else
      return nil
    end
  end
  
  def user_count
    self.users.count
  end
  
  def punish(chore)
    # Social Media Penalty
    chore.user.post(self.facebook_penalty_msg) if self.facebook_penalty
    
    # nothing currently setup for monetary penalty
    
    # Mass Email Penalty
    GroupMailer.email_penalty(chore).deliver if self.email_penalty
  end
  
  def chore_history
    assigned_chores = self.assigned_chores.where('due_date < ?', Date.today).where("due_date > ?", Date.today - 3.month).order('due_date ASC, username ASC, name ASC')
  end
  
  def possible_chore_trades(user_id)
    assigned_chores = self.assigned_chores.where("user_id != ? AND status = ?", user_id, 'Not Completed').where("start_date <= ? and due_date >= ?", Date.today, Date.today).order('name ASC, username ASC')
  end
  
  def recently_completed_chores(user_id)
    chores = self.assigned_chores.joins('LEFT OUTER JOIN ratings ON (ratings.assigned_chore_id = assigned_chores.id AND ratings.user_id = ' + user_id.to_s + ')')
      .where("completion_date > NOW() - interval '7 days'")
      .where("assigned_chores.user_id != ? AND (ratings.archived = ? OR ratings.archived IS ?)", user_id, false, nil).order('name ASC, username ASC')
    unless chores.empty?
      chores
    else
      nil
    end
  end
  
  def pending_trades
    trades = self.trades.where(:status => 'Pending')
    unless trades.empty?
      trades
    else
      nil
    end
  end
  
  def chores_completed_by_week
    self.assigned_chores.select("date_trunc('week',due_date) as date,COUNT(completion_date) as count").where("due_date < ?", Time.now).where("completion_date is not null").group("date_trunc('week',due_date)").order("date_trunc('week',due_date) ASC")
  end
  
  def percentage_chores_completed_by_user
    self.assigned_chores.select("username, ROUND(COUNT(completion_date)/COUNT(id)::decimal * 100,2) as percent").where("due_date < ?", Time.now).group("username")
  end
  
  def percentage_completed_by_chore
    self.assigned_chores.select("name, ROUND(COUNT(completion_date)/COUNT(id)::decimal * 100,2) as percent").where("due_date < ?", Time.now).group("name")
  end
end
