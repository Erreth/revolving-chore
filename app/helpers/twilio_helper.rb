module TwilioHelper
  @@account_sid = "ACc294deba38290a03281a1b551db950a1"
  @@auth_token = "961d778164fe5170404601655495c1e7"
  @@from_number = "+19518014646"
  
  def text(number, message)
    @client = Twilio::REST::Client.new @@account_sid, @@auth_token
    begin
      @client.account.sms.messages.create(
        :from => @@from_number,
        :to => number,
        :body => message
      )
    rescue => api_error
      logger.error api_error.message
    end
  end
end