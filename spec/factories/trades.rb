# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :trade do
    from_user_id 1
    from_user_name "MyString"
    from_chore_id 1
    from_chore_name "MyString"
    to_user_id 1
    to_user_name "MyString"
    to_chore_id 1
    to_chore_name "MyString"
    status "MyString"
    accept_date "2013-06-28 20:53:39"
    expiration_date "2013-06-28 20:53:39"
  end
end
