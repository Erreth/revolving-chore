describe ChoresController do
  describe "#new" do
    it "routes to chores#new" do
      #{ :get => "/groups/1/chores/new" }.should route_to(
       # :controller => "chores",
        #:action => "new" 
      #)
      assert_routing({ :path => "/groups/1/chores/new", :method => :post },
        { :controller => "chores", :action => "new" })
    end
  end
end