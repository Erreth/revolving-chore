require 'spec_helper'

describe AssignedChore do

  describe 'find insert date' do
    before :each do
      @lloyd_house = FactoryGirl.create(:group)
      @sweeping = FactoryGirl.create(:assigned_chore)
    end
    describe 'create initial chores method' do
      it 'should return todays date if group does not have a chore start date' do
        AssignedChore.find_insert_date('create_initial_chores', @lloyd_house.id).should == Date.today
      end
      it 'should return the groups chore start date if the chore start date is greater than or equal to today' do
        @lloyd_house.update_attribute(:chore_start_date, (Date.today + 1))
        AssignedChore.find_insert_date('create_initial_chores', @lloyd_house.id).should == @lloyd_house.chore_start_date
      end
      it 'should return the start date of the current cycles assigned chore if there is a current assigned chore' do
        @sweeping.update_attribute(:start_date, Date.today)
        @lloyd_house.update_attribute(:chore_start_date, (Date.today - 1))
        AssignedChore.find_insert_date('create_initial_chores', @lloyd_house.id).should == @sweeping.start_date
      end
    end
    describe 'add more chores method' do
      it 'should return the due date of the last assigned chore + 1 day' do
        @sweeping.update_attributes(:group_id => @lloyd_house.id, :due_date => Date.today)
        AssignedChore.find_insert_date('add_more_chores', @lloyd_house.id).should == @sweeping.due_date + 1
      end
    end
  end
  describe 'delete assigned chores' do
    before :each do
      @lloyd_house = FactoryGirl.create(:group)
      @sweeping = FactoryGirl.create(:assigned_chore, :start_date => Date.today, :group_id => @lloyd_house.id)
      @mopping = FactoryGirl.create(:assigned_chore, :name => 'Mopping', :start_date => Date.today, :group_id => @lloyd_house.id)
    end
    it 'should delete all current assigned chores' do      
      @sweeping.update_attribute(:due_date, Date.today)
      @mopping.update_attribute(:due_date, Date.today)

      AssignedChore.delete_assigned_chores(@lloyd_house.id)
      AssignedChore.find_by_id(@sweeping.id).should == nil
      AssignedChore.find_by_id(@mopping.id).should == nil
    end
    it 'should delete all future assigned chores' do
      @sweeping.update_attribute(:due_date, Date.today + 7)
      @mopping.update_attribute(:due_date, Date.today + 20)
      
      AssignedChore.delete_assigned_chores(@lloyd_house.id)
      AssignedChore.find_by_id(@sweeping.id).should == nil
      AssignedChore.find_by_id(@mopping.id).should == nil
    end
  end
  describe 'create chore order method' do
    #this method requires that there is at least one user in the group
    before :each do
      @felix = FactoryGirl.create(:user)
      @lloyd_house = FactoryGirl.create(:group)
      @critique = FactoryGirl.create(:chore, :group_id => @lloyd_house.id)
      GroupMap.new(:group_id => @lloyd_house.id, :user_id => @felix.id).save
    end
    it 'should return an array[0] with a sub array of chore ids' do
      chore_order = AssignedChore.create_chore_order(@lloyd_house.id)
      chore_order[0].is_a?(Array).should == true
    end
    it 'should return an array[1] with a hash of chore names' do
      chore_order = AssignedChore.create_chore_order(@lloyd_house.id)
      chore_order[1].is_a?(Hash).should == true
    end
    describe 'with an equal number of users and chores' do
      it 'should return a single dimensional array of chore ids' do
        chore_order = AssignedChore.create_chore_order(@lloyd_house.id)
        chore_order[0].each do |element|
          element.is_a?(Array).should_not == true
        end
      end
      it 'should return a hash with chore id as a key and the chore name as value' do
        chore_order = AssignedChore.create_chore_order(@lloyd_house.id)
        chore_order[1][@critique.id].should == @critique.name          
      end
    end
    describe 'with more chores than users' do
      before :each do
        @snark = FactoryGirl.create(:chore, :name => 'Snark', :group_id => @lloyd_house.id)
      end
      it 'should assign multiple chores to a single user by returning a multidimensional array in chore[0]' do
        chore_order = AssignedChore.create_chore_order(@lloyd_house.id)
        chore_order[0][0].is_a?(Array).should == true
      end
    end
    describe 'with less chores than users' do
      before :each do
        @ben = FactoryGirl.create(:user, :username => 'Ben')
        GroupMap.new(:group_id => @lloyd_house.id, :user_id => @ben.id).save
      end 
      it 'should have a chore id of 0 in the chore_order[0]' do
        chore_order = AssignedChore.create_chore_order(@lloyd_house.id)
        chore_order[0].include?(0).should == true
      end
      it 'should have an a key => value of (0 => No Chore) in the chore_order[1] hash' do
        chore_order = AssignedChore.create_chore_order(@lloyd_house.id)
        chore_order[1].has_key?(0).should == true
        chore_order[1][0].should == 'No Chore'
      end
    end

  end
  describe 'assign chores method' do
    before :each do
      @felix = FactoryGirl.create(:user)
      @lloyd_house = FactoryGirl.create(:group)
      @critique = FactoryGirl.create(:chore, :group_id => @lloyd_house.id)
      GroupMap.new(:group_id => @lloyd_house.id, :user_id => @felix.id).save
      @chore_order = [[@critique.id], {@critique.id => @critique.name}]
    end
    it 'should create a chore for the starting date' do
      AssignedChore.assign_chores(@lloyd_house.id, @chore_order, Date.today)
      AssignedChore.where(:start_date => Date.today).all.empty?.should_not == true
    end
    it 'should create many cycles worth of chores as there are users so that each user gets each chore or set of chores once' do
      AssignedChore.assign_chores(@lloyd_house.id, @chore_order, Date.today)
      number_of_cycles = @lloyd_house.users.count
      AssignedChore.where(:username => @felix.username).all.count.should == number_of_cycles
    end
    describe 'with more chores than users' do
      before :each do
        @snark = FactoryGirl.create(:chore, :name => 'Snark', :group_id => @lloyd_house.id)
        @chore_order = [[[@critique.id, @snark.id]], {@critique.id => @critique.name, @snark.id => @snark.name}] 
      end
      it 'should assign multiple chores to one user' do
        AssignedChore.assign_chores(@lloyd_house.id, @chore_order, Date.today)
        number_of_chores = @lloyd_house.chores.count
        AssignedChore.where(:username => @felix.username).all.count.should == number_of_chores
      end
    end
    describe 'with less chores than users' do
      before :each do
        @ben = FactoryGirl.create(:user, :username => 'Ben')
        GroupMap.new(:group_id => @lloyd_house.id, :user_id => @ben.id).save
        @chore_order = [[@critique.id, 0], {@critique.id => @critique.name, 0 => 'No Chore'}] 
      end 
      it 'should assign No Chore to a user' do
        AssignedChore.assign_chores(@lloyd_house.id, @chore_order, Date.today)
        AssignedChore.where(:name => 'No Chore').all.should_not == nil
      end
    end
  end
  describe 'create initial assigned chores' do
    before :each do
      @felix = FactoryGirl.create(:user)
      @lloyd_house = FactoryGirl.create(:group, :chore_period_length_days => 7, :chore_start_date => Date.today)
      @critique = FactoryGirl.create(:chore, :group_id => @lloyd_house.id)
      GroupMap.new(:group_id => @lloyd_house.id, :user_id => @felix.id).save
    end
    describe 'with a group of less than or equal to 7 users' do
      it 'should update the groups assign_more_chores_date to 2 * chore_period_length_days from the today' do
        AssignedChore.create_initial_assigned_chores(@lloyd_house.id)
        @lloyd_house = Group.find(@lloyd_house.id)
        @lloyd_house.assign_more_chores_date.should == (Date.today + (@lloyd_house.chore_period_length_days * 2))
      end    
    end
    describe 'with a group of more than 7 users' do
      before :each do
        @ben = FactoryGirl.create(:user, :username => 'Ben')
        @harrison = FactoryGirl.create(:user, :username => 'Harrison')
        @brad = FactoryGirl.create(:user, :username => 'Brad')
        @corey = FactoryGirl.create(:user, :username => 'Corey')
        @kaleb = FactoryGirl.create(:user, :username => 'Kaleb')
        @matt = FactoryGirl.create(:user, :username => 'Matt')
        @jacob = FactoryGirl.create(:user, :username => 'Jacob')
        GroupMap.new(:group_id => @lloyd_house.id, :user_id => @felix.id).save
        GroupMap.new(:group_id => @lloyd_house.id, :user_id => @ben.id).save
        GroupMap.new(:group_id => @lloyd_house.id, :user_id => @harrison.id).save
        GroupMap.new(:group_id => @lloyd_house.id, :user_id => @brad.id).save
        GroupMap.new(:group_id => @lloyd_house.id, :user_id => @corey.id).save
        GroupMap.new(:group_id => @lloyd_house.id, :user_id => @kaleb.id).save
        GroupMap.new(:group_id => @lloyd_house.id, :user_id => @matt.id).save
        GroupMap.new(:group_id => @lloyd_house.id, :user_id => @jacob.id).save
      end
      it 'should update the group assign_more_chores_date to (# of cycles - 7) * chore_period_length_days from today' do
        AssignedChore.create_initial_assigned_chores(@lloyd_house.id)
        @lloyd_house = Group.find(@lloyd_house.id)
        cycles = @lloyd_house.users.count
        chore_period_length = @lloyd_house.chore_period_length_days
        @lloyd_house.assign_more_chores_date.should == (Date.today + ((cycles - 7) * chore_period_length))
      end
    end
  end
end
