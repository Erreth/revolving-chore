require 'spec_helper'

describe User do
  describe 'set up with omniauth' do
    before :each do
      @auth = {
        "provider" => 'facebook', 
        "uid" => 'test', 
        "credentials" => {"token" => 'token', "secret" => 'secret'},
        "info" => {"name" => 'Felix', "email" => 'fhuang@westmont.edu'}
        }
    end
    describe 'create with omniauth' do
      it 'should create a user' do
        User.create_with_omniauth(@auth).username.should == 'Felix'
      end
    end
    describe 'find and update with omniauth' do
      it 'should find user when available' do
        @felix = FactoryGirl.create(:user, :uid => "test", :provider => "facebook")
        User.find_and_update_with_omniauth(@auth).uid.should == "test"
      end
      it 'should update user if found' do
        @felix = FactoryGirl.create(:user, :uid => "test", :provider => "facebook")
        User.find_and_update_with_omniauth(@auth).username.should == "Felix"
      end
      it 'should return nil when user isnt found' do
        User.find_and_update_with_omniauth(@auth).should be_nil
      end
    end
  end
  describe 'post' do
    describe 'facebook' do
      before :each do
        @felix = FactoryGirl.create(:user, :provider => "facebook", :auth_token => "test_token")
      end
      it 'should create a new koala api session' do
        Koala::Facebook::API.any_instance.stub(:put_wall_post)
        Koala::Facebook::API.should_receive(:new).with("test_token")
        @felix.post("Hello World")
      end
      it 'should call method to post to wall' do
        pending 'implementation'
      end
    end
    describe 'twitter' do
      before :each do
        @felix = FactoryGirl.create(:user, 
        :provider => "twitter", 
        :auth_token => "test_token", 
        :auth_secret => "test_secret")
      end
      it 'should create a new twitter api session' do
        twitter_client_mock.stub(:update)
        Twitter::Client.should_receive(:new).with(:oauth_token => "test_token", :oauth_token_secret => "test_secret").and_return(mock("twitter_client_mock"))
        @felix.post("Hello World")
      end
      it 'should call method to post to thread' do
        pending 'implementation'
      end
    end
  end
  describe 'send_notification' do
    pending 'implementation'
  end
  describe 'set_calendar_colors' do
    pending 'implementation'
  end
end
