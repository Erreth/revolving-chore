require 'spec_helper'

describe Group do
  before :each do
    @lloyd_house = FactoryGirl.create(:group)
  end
  describe 'create new group' do
  	it 'should be able to create an instance of the Group model' do
      @moreno = FactoryGirl.create(:group, :name => 'Moreno')
      @moreno.is_a?(Group).should == true
    end 
  end

  describe 'save a group' do
  	it 'should be able to save an instance of a Group' do
      @moreno = FactoryGirl.build(:group, :name => 'Moreno')
      @moreno.save 
      Group.find(@moreno.id).should == @moreno
    end
  end

  describe 'destroy groups' do
    it 'should be able to destroy an instance of a group' do
      lloyd_house_id = @lloyd_house.id
      @lloyd_house.destroy
      expect { Group.find(lloyd_house_id) }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
  describe 'set calendar colors method' do
    it 'should return event strips with colors added to the events' do
      #needs to have at least 1 user in the group
      @felix = FactoryGirl.create(:user)
      GroupMap.new(:group_id => @lloyd_house.id, :user_id => @felix.id).save
      @sweeping = FactoryGirl.create(:assigned_chore, :username => @felix.username)
      @kitchen = FactoryGirl.create(:assigned_chore, :name => 'Kitchen', :username => @felix.username)
      
      event_strips = [[nil, @sweeping, nil], [nil, nil, nil, @kitchen]]
      colored_event_strips = @lloyd_house.set_calendar_colors(event_strips)
      colored_event_strips[0][1].color.is_a?(String).should == true
    end
  end
  describe 'current cycles chores method' do
    it 'should return an array of this cycles assigned chores' do
      @lloyd_house.update_attribute(:chore_period_length_days, 7)
      @sweeping = FactoryGirl.create(:assigned_chore, :due_date => (Date.today +1), :group_id => @lloyd_house.id)
      @kitchen = FactoryGirl.create(:assigned_chore, :name => 'Kitchen', :due_date => (Date.today + 5), :group_id => @lloyd_house.id) 

      @lloyd_house.current_cycles_chores.should == [@kitchen, @sweeping]
    end
  end
end
