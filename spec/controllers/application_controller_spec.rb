require 'spec_helper'

describe ApplicationController do
  controller do
    def error
      raise ApplicationController::AccessDenied
    end
    def index
      render :nothing => true
    end
  end
  
  describe "handling AccessDenied exceptions" do
    it "redirects to the /401.html page" do
      routes.draw { get "error" => "anonymous#error" }
      session[:user_id] = FactoryGirl.create(:user).id
      get :error
      expect(response).to redirect_to("/401.html")
    end
  end
  
  describe 'set_current_user' do
    it 'should populate @current_user if available' do
      @felix = FactoryGirl.create(:user)
      session[:user_id] = @felix.id
      get :index
      assigns(:current_user).should == @felix
    end
    it 'should set session[:action] if no user is found' do 
      get :index
      cookies[:action].should_not be_nil 
    end
    it 'should redirect to login_path if no user is found' do
      get :index
      response.should redirect_to(home_login_path)
    end
  end
  
  describe 'set_current_env' do
    it 'should populate @current_env' do
      session[:user_id] = FactoryGirl.create(:user).id
      Rails.stub(:env).and_return('developer')
      get :index
      assigns(:current_env).should == 'developer'
    end
  end
end


