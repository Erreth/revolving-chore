require 'spec_helper'

describe GroupsController do
  describe 'new' do
  	it 'should generate a new instance of the Group model' do 
  		assert group = Group.new
  	end
  	it 'should send the instance of Group model to the Edit Group page' do
  		group = Group.new
      assert_routing({ :path => "/groups/new", :method => :get },
        { :controller => "groups", :action => "new" })
  		#need to test for sending the instance to this path
  	end
  end
  describe 'create' do
    before :each do
      @felix = FactoryGirl.create(:user)
    end  
  	it 'should call the model method to create a new group' do
  	  pending 'implementation'
  	end
  	it 'should call the model method to add the current user to the group' do
  	  pending 'implementation'
  	end
  	it 'should redirect to the new view when missing required parameters' do
  	  pending 'implementation'
  	end
  end
end
