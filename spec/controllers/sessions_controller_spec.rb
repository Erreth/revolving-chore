require 'spec_helper'

describe SessionsController do
  describe 'create' do
    before :each do
      @auth = {:provider => 'test_provider', :uid => 'test_uid'}
      request.env["omniauth.auth"] = @auth
      @felix = FactoryGirl.create(:user)
    end    
    it 'should call the model method to find the user' do
      User.should_receive(:find_by_provider_and_uid).with("test_provider","test_uid").and_return(@felix)
      post :create, :provider => "facebook"
    end 
    it 'should call the model method that inserts a new user if no record is found' do
      User.stub(:find_by_provider_and_uid).and_return(nil)
      User.should_receive(:create_with_omniauth).with(@auth).and_return(@felix)
      post :create, :provider => "facebook"
    end
    it 'should select the user dashboard template for rendering if no action specified' do
      User.stub(:find_by_provider_and_uid).with("test_provider","test_uid").and_return(@felix)
      post :create, :provider => "facebook"
      response.should redirect_to(user_path(@felix.id))
    end
    it 'should redirect to the action specified if present' do
      User.stub(:find_by_provider_and_uid).with("test_provider","test_uid").and_return(@felix)
      cookies[:action] = '/groups/2/invitations/3/accept'
      post :create, :provider => "facebook"
      response.should redirect_to('/groups/2/invitations/3/accept')
    end
  end
  
  describe 'destroy' do
    pending 'implementation'
  end
end