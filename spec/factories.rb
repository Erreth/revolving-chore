FactoryGirl.define do
  factory :user do
    username "Fefe"
    display_name  "Felix Huang"
    notification_method "SMS"
    monetary_amount_due "0.00"
    provider "Twitter"
    email "fhuang@westmont.edu"
  end
  
  factory :group do
    name "Lloyd House"
    chore_period_length_days 7
    chore_start_date Date.today
  end
    
  factory :chore do
    name "Critique"
    description "Analyze in a moody way."
  end

  factory :invitation do
    group_id 1
    email 'ben.vanderwall@gmail.com'
    group_name 'Lloyd House'
    accepted false
  end

  factory :assigned_chore do
    name 'Sweeping'
    start_date Date.today
  end
end