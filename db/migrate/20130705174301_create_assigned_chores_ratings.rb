class CreateAssignedChoresRatings < ActiveRecord::Migration
  def change
    create_table :assigned_chores_ratings do |t|
      t.integer :assigned_chore_id
      t.integer :user_id
      t.string :user_name
      t.decimal :rating, :scale => 2
    end
  end
end
