class AddColorToAssignedChores < ActiveRecord::Migration
  def change
    add_column :assigned_chores, :color, :string
  end
end
