class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :password
      t.string :salt
      t.string :display_name
      t.string :notification_method
      t.float :monetary_amount_due

      t.timestamps
    end
  end
end
