class AddEmailPenaltyToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :email_penalty, :boolean
    add_column :groups, :email_penalty_address, :string
    add_column :groups, :email_penalty_msg, :text
  end
end
