class RenameAssignedChoresRatingsToRatings < ActiveRecord::Migration
  def change
      rename_table :assigned_chores_ratings, :ratings
  end
end
