class CreateGroupMaps < ActiveRecord::Migration
  def change
    create_table :group_maps do |t|
      t.references :user
      t.references :group

      t.timestamps
    end
    add_index :group_maps, :user_id
    add_index :group_maps, :group_id
  end
end
