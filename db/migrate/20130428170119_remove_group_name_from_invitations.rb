class RemoveGroupNameFromInvitations < ActiveRecord::Migration
  def up
    remove_column :invitations, :group_name
  end

  def down
    add_column :invitations, :group_name, :string
  end
end
