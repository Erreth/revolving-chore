class CreateAssignedChores < ActiveRecord::Migration
  def change
    create_table :assigned_chores do |t|
      t.references :user
      t.references :group
      t.references :chore
      t.string :name
      t.text :description
      t.integer :verified_by
      t.float :monetary_penalty_value
      t.string :status
      t.boolean :posted_on_facebook
      t.date :start_date
      t.date :due_date

      t.timestamps
    end
    add_index :assigned_chores, :user_id
    add_index :assigned_chores, :group_id
    add_index :assigned_chores, :chore_id
  end
end
