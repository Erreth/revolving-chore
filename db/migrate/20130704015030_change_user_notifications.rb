class ChangeUserNotifications < ActiveRecord::Migration
  class User < ActiveRecord::Base
  end

  def change
    remove_column :users, :notification_method
    add_column :users, :email_notification, :boolean
    add_column :users, :sms_notification, :boolean
    add_column :users, :phone_number, :string
    
    User.reset_column_information
    User.update_all email_notification: true
  end
end
