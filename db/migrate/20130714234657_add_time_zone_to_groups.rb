class AddTimeZoneToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :time_zone, :string
  end
end
