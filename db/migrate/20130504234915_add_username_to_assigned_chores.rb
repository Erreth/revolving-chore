class AddUsernameToAssignedChores < ActiveRecord::Migration
  def change
    add_column :assigned_chores, :username, :string
  end
end
