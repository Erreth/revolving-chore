class AddCompletedDateToAssignedChores < ActiveRecord::Migration
  def change
    add_column :assigned_chores, :completion_date, :datetime
  end
end
