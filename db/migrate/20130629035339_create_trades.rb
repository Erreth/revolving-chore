class CreateTrades < ActiveRecord::Migration
  def change
    create_table :trades do |t|
      t.integer :from_user_id
      t.string :from_user_name
      t.integer :from_chore_id
      t.string :from_chore_name
      t.integer :to_user_id
      t.string :to_user_name
      t.integer :to_chore_id
      t.string :to_chore_name
      t.string :status
      t.datetime :accept_date
      t.datetime :expiration_date

      t.timestamps
    end
  end
end
