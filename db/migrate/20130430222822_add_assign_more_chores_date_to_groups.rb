class AddAssignMoreChoresDateToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :assign_more_chores_date, :date
  end
end
