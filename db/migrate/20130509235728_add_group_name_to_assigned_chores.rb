class AddGroupNameToAssignedChores < ActiveRecord::Migration
  def change
    add_column :assigned_chores, :group_name, :string
  end
end
