class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name
      t.boolean :facebook_penalty
      t.text :facebook_penalty_msg
      t.boolean :monetary_penalty
      t.float :monetary_penalty_value
      t.integer :chore_period_length_days

      t.timestamps
    end
  end
end
