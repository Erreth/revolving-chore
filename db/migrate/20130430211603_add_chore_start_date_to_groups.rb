class AddChoreStartDateToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :chore_start_date, :date
  end
end
