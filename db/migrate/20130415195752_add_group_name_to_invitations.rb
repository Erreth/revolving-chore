class AddGroupNameToInvitations < ActiveRecord::Migration
  def change
    add_column :invitations, :group_name, :string
  end
end
