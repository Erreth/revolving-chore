class CreateChores < ActiveRecord::Migration
  def change
    create_table :chores do |t|
      t.string :name
      t.text :description
      t.references :group

      t.timestamps
    end
    add_index :chores, :group_id
  end
end
