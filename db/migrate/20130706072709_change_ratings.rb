class ChangeRatings < ActiveRecord::Migration
  def change
    add_column :ratings, :archived, :boolean, default: false
    rename_column :ratings, :rating, :score
  end
end
