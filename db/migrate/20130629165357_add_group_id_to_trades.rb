class AddGroupIdToTrades < ActiveRecord::Migration
  def change
    add_column :trades, :group_id, :integer
  end
end
