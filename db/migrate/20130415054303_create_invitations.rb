class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.references :group
      t.string :email

      t.timestamps
    end
    add_index :invitations, :group_id
  end
end
