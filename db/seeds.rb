# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
felix = User.new(:id => 1, :username => "Felix Huang")
felix.provider = "developer"
felix.uid = "Felix Huang"
felix.save

jake = User.new(:id => 2, :username => "Jake Miller")
jake.provider = "developer"
jake.uid = "Jake Miller"
jake.save

kaleb = User.new(:id => 3, :username => "Kaleb Madel")
kaleb.provider = "developer"
kaleb.uid = "Kaleb Madel"
kaleb.save

ben = User.new(:id => 4, :username => "Ben VanderWall")
ben.provider = "developer"
ben.uid = "Ben VanderWall"
ben.save

lloyd = Group.create(:id => 1, :name => "Lloyd House", :chore_period_length_days => 7)
felix.groups << lloyd

lloyd.chores << Chore.create(:id => 1, :name => "Clean the Kitchen")
lloyd.chores << Chore.create(:id => 2, :name => "Mop")
lloyd.chores << Chore.create(:id => 3, :name => "Sweep")
lloyd.chores << Chore.create(:id => 4, :name => "Vacuum")
lloyd.chores << Chore.create(:id => 5, :name => "Clean the Bathroom")
lloyd.chores << Chore.create(:id => 6, :name => "Pick up the Living Room")
lloyd.chores << Chore.create(:id => 7, :name => "Thrust")
lloyd.chores << Chore.create(:id => 8, :name => "Clean the Deck")


