# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130714234657) do

  create_table "assigned_chores", :force => true do |t|
    t.integer  "user_id"
    t.integer  "group_id"
    t.integer  "chore_id"
    t.string   "name"
    t.text     "description"
    t.integer  "verified_by"
    t.float    "monetary_penalty_value"
    t.string   "status"
    t.boolean  "posted_on_facebook"
    t.date     "start_date"
    t.date     "due_date"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.string   "username"
    t.string   "color"
    t.string   "group_name"
    t.datetime "completion_date"
  end

  add_index "assigned_chores", ["chore_id"], :name => "index_assigned_chores_on_chore_id"
  add_index "assigned_chores", ["group_id"], :name => "index_assigned_chores_on_group_id"
  add_index "assigned_chores", ["user_id"], :name => "index_assigned_chores_on_user_id"

  create_table "chores", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "group_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "chores", ["group_id"], :name => "index_chores_on_group_id"

  create_table "group_maps", :force => true do |t|
    t.integer  "user_id"
    t.integer  "group_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "group_maps", ["group_id"], :name => "index_group_maps_on_group_id"
  add_index "group_maps", ["user_id"], :name => "index_group_maps_on_user_id"

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.boolean  "facebook_penalty"
    t.text     "facebook_penalty_msg"
    t.boolean  "monetary_penalty"
    t.float    "monetary_penalty_value"
    t.integer  "chore_period_length_days"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.string   "chore_sort_type"
    t.date     "chore_start_date"
    t.date     "assign_more_chores_date"
    t.boolean  "email_penalty"
    t.string   "email_penalty_address"
    t.text     "email_penalty_msg"
    t.string   "time_zone"
  end

  create_table "invitations", :force => true do |t|
    t.integer  "group_id"
    t.string   "email"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.boolean  "accepted"
    t.string   "invitee_name"
  end

  add_index "invitations", ["group_id"], :name => "index_invitations_on_group_id"

  create_table "ratings", :force => true do |t|
    t.integer "assigned_chore_id"
    t.integer "user_id"
    t.string  "user_name"
    t.decimal "score"
    t.boolean "archived",          :default => false
  end

  create_table "trades", :force => true do |t|
    t.integer  "from_user_id"
    t.string   "from_user_name"
    t.integer  "from_chore_id"
    t.string   "from_chore_name"
    t.integer  "to_user_id"
    t.string   "to_user_name"
    t.integer  "to_chore_id"
    t.string   "to_chore_name"
    t.string   "status"
    t.datetime "accept_date"
    t.datetime "expiration_date"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "group_id"
  end

  create_table "users", :force => true do |t|
    t.string   "username"
    t.string   "display_name"
    t.float    "monetary_amount_due"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "provider"
    t.string   "uid"
    t.string   "auth_token"
    t.date     "auth_expiration_date"
    t.string   "email"
    t.string   "auth_secret"
    t.boolean  "email_notification"
    t.boolean  "sms_notification"
    t.string   "phone_number"
  end

end
